# -*- coding: utf-8 -*-
{
    'name': "CSSA Partners",

    'summary': """
        Una serie di strumenti per ottimizzare il lavoro di un Centro Servizi""",

    'description': """
        Long description of module's purpose
    """,

    'author': "CSSA Partners",
    'website': "http://www.cssapartners.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Centro Servizi',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'crm', 'mail', 'sale_management', 'account', 'project', 'hr', 'gws_google_maps', 'gtica_whatsapp_integration_crm'],

    'qweb': [
        'static/xml/list_view.xml'
    ],

    # always loaded
    'data': [
        'security/cssa_security.xml',
        'security/ir.model.access.csv',
        'views/zones.xml',
        'views/templates.xml',
        'views/res_partner.xml',
        'views/sale_order.xml',
        'views/calendar_event_form.xml',
        'views/mail_activity_form_popup.xml',
        'views/mail_template.xml',
        'views/crm_lead_tree.xml',
        'views/crm_lead_tree_opportunity.xml',
        'views/product_form.xml',
        'views/bank_form.xml',
        'wizards/hotel_stay.xml',
        'views/crm_lead.xml',
        'emails/request_hotel_stay_template.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True
}
