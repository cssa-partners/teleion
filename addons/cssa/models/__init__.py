# -*- coding: utf-8 -*-

from . import res_partner
from . import crm_lead
from . import zones
from . import completed_zones
from . import calendar
from . import cssa
from . import mail_activity
from . import crm_activity_report
from . import sale_order
from . import mail_mail
from . import res_bank
from . import mail_thread
from . import product_product