# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools
from datetime import date
import urllib.parse

import threading
import openerp
import openerp.release
import openerp.sql_db
import openerp.tools


class Lead(models.Model):
    _inherit = 'crm.lead'

    to_rework = fields.Boolean('To rework', default=False)

    dipendenti = fields.Char('N° Dipendenti')
    fatturato = fields.Float('Fatturato')
    region = fields.Char('Regione')
    provincia = fields.Char('Provincia')
    settore = fields.Char('Settore')
    ateco = fields.Char("ATECO")
    area_interesse = fields.Char("Area d'interessse")
    categoria_interesse = fields.Char("Categoria d'interesse")

    posizione_lavorativa = fields.Selection([
        ('CEO - SRL/SPA','CEO - SRL/SPA'),
        ('AMMINISTRATORE UNICO - SRL/SPA','AMMINISTRATORE UNICO - SRL/SPA'),
        ('SOCIO ACCOMANDATARIO - SAS','SOCIO ACCOMANDATARIO - SAS'),
        ('PRESIDENTE - SRL/SPA','PRESIDENTE - SRL/SPA'),
        ('TITOLARE - DITTA INDIVIDUALE','TITOLARE - DITTA INDIVIDUALE'),
        ('SOCIO - SNC','SOCIO - SNC'),
        ('AMMINISTRATORE DELEGATO - SRL/SPA','AMMINISTRATORE DELEGATO - SRL/SPA'),
        ('UFF. ACQUISTI','UFF. ACQUISTI'),
        ('UFF. TECNICO','UFF. TECNICO'),
        ('SEGRETARIA/O','SEGRETARIA/O'),
        ('RESP. LABORATORIO','RESP. LABORATORIO'),
        ('RESP. PRODUZIONE','RESP. PRODUZIONE'),
        ('UFF. FORNITORI','UFF. FORNITORI'),
        ('DIPENDENTE','DIPENDENTE'),
        ('LABORATORIO','LABORATORIO'),
        ('ARCHITETTO','ARCHITETTO'),
        ('RESP. INTERIOR','RESP. INTERIOR')
    ])

    forma_societaria = fields.Selection([
        ('Ditta Individuale','Ditta Individuale'),
        ('SNC','SNC'),
        ('SAS','SAS'),
        ('SRL','SRL'),
        ('SRLS','SRLS'),
        ('SPA','SPA')

    ], 'Forma Societaria')

    partita_iva = fields.Char('Partita IVA')
    codice_fiscale = fields.Char('Codice Fiscale')

    in_out = fields.Char('IN/OUT')
    for_admin = fields.Boolean('Amministrazione')

    activity_orario = fields.Char('Orario')
    call_count = fields.Integer('Call Count')
    check_count = fields.Integer('Check Count')
    go_count = fields.Integer('Go Count')

    orario = fields.Char('Orario')

    now = fields.Char()

    date_last_appointment = fields.Datetime('Data ultimo appuntamento')

    @api.model
    def create(self, vals):
        context = dict(self._context or {})
        if vals.get('provincia'):
            context['provincia'] = vals.get('provincia')
            self.partner_id.provincia = vals.get('provincia')
        if vals.get('region'):
            context['region'] = vals.get('region')
            self.partner_id.region = vals.get('region')
        if vals.get('partita_iva'):
            context['vat'] = vals.get('partita_iva')
            self.partner_id.partita_iva = vals.get('partita_iva')
            self.partner_id.vat = vals.get('partita_iva')
        if vals.get('settore'):
            context['settore'] = vals.get('settore')
            self.partner_id.settore = vals.get('settore')
        if vals.get('ateco'):
            context['ateco'] = vals.get('ateco')
            self.partner_id.ateco = vals.get('ateco')
        if vals.get('area_interesse'):
            context['area_interesse'] = vals.get('area_interesse')
            self.partner_id.area_interesse = vals.get('area_interesse')
        if vals.get('categoria_interesse'):
            context['categoria_interesse'] = vals.get('categoria_interesse')
            self.partner_id.categoria_interesse = vals.get('categoria_interesse')
        if vals.get('dipendenti'):
            context['dipendenti'] = vals.get('dipendenti')
            self.partner_id.dipendenti = vals.get('dipendenti')
        if vals.get('fatturato'):
            context['fatturato'] = vals.get('fatturato')
            self.partner_id.fatturato = vals.get('fatturato')

        lead = super(Lead, self).create(vals)

        return lead

    def write(self, vals):
        if vals.get('website'):
            vals['website'] = self.env['res.partner']._clean_website(vals['website'])

        if vals.get('region'):
            self.partner_id.region = vals.get('region')
        if vals.get('provincia'):
            self.partner_id.provincia = vals.get('provincia')
        if vals.get('settore'):
            self.partner_id.settore = vals.get('settore')
        if vals.get('ateco'):
            self.partner_id.ateco = vals.get('ateco')
        if vals.get('area_interesse'):
            self.partner_id.area_interesse = vals.get('area_interesse')
        if vals.get('categoria_interesse'):
            self.partner_id.categoria_interesse = vals.get('categoria_interesse')
        if vals.get('fatturato'):
            self.partner_id.fatturato = vals.get('fatturato')
        if vals.get('n_dipendenti'):
            self.partner_id.n_dipendenti = vals.get('n_dipendenti')
        if vals.get('partita_iva'):
            self.partner_id.partita_iva = "IT"+vals.get('partita_iva')
            self.partner_id.vat = "IT"+vals.get('partita_iva')
        if vals.get('codice_fiscale'):
            self.partner_id.l10n_it_codice_fiscale = vals.get('codice_fiscale')
        if vals.get('email_from'):
            self.partner_id.email = vals.get('email_from')
        if vals.get('phone'):
            self.partner_id.phone = vals.get('phone')
        if vals.get('name'):
            self.partner_id.name = vals.get('name').upper()
        if vals.get('forma_societaria'):
            if vals.get('forma_societaria') not in self.partner_id.name:
                self.partner_id.name = self.partner_id.name + " " + vals.get('forma_societaria')

        # stage change: update date_last_stage_update
        if 'stage_id' in vals:
            vals['date_last_stage_update'] = fields.Datetime.now()
            stage_id = self.env['crm.stage'].browse(vals['stage_id'])

            #self.manage_in_out(stage_id)

            if stage_id.is_won:
                vals.update({'probability': 100})
                # self.esito = False
                #
                # if vals.get('stage_id') == 4 or vals.get('stage_id') == 8:
                #     self.notify('offerta', None)


        # Only write the 'date_open' if no salesperson was assigned.
        if vals.get('user_id') and 'date_open' not in vals and not self.mapped('user_id'):
            vals['date_open'] = fields.Datetime.now()
        # stage change with new stage: update probability and date_closed
        if vals.get('probability', 0) >= 100 or not vals.get('active', True):
            vals['date_closed'] = fields.Datetime.now()
        elif 'probability' in vals:
            vals['date_closed'] = False
        if vals.get('user_id') and 'date_open' not in vals:
            vals['date_open'] = fields.Datetime.now()

        write_result = super(Lead, self).write(vals)
        # Compute new automated_probability (and, eventually, probability) for each lead separately
        if self._should_update_probability(vals):
            self._update_probability()

        # if vals.get('tecnico'):
        #     # update appointments if any
        #     self.update_appointment_owner()

        return write_result


    def get_directions(self):
        query = self.partner_id.city+","+self.partner_id.street+","+self.partner_id.zip
        encoded_query = urllib.parse.quote(query)
        google_map_url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&destination="+encoded_query

        return {
            'name'     : 'Go to website',
            'res_model': 'ir.actions.act_url',
            'type'     : 'ir.actions.act_url',
            'url'      : google_map_url
       }

    # def notify(self, type, vals):
    #     if type == 'offerta':
    #         message = "C'è stata una nuova Offerta!"
    #         users = []
    #         groups=self.sudo().env['res.groups'].search([])
    #         for group in groups:
    #             if group.category_id.name == 'Offerta' and group.name == 'User':
    #                 users = group.users
    #                 break
    #
    #         user_ids = []
    #         for user in users:
    #             user_ids.append(user.id)
    #
    #         user_ids = (tuple(user_ids))
    #         self.message_notify(
    #             partner_ids=user_ids,
    #             subject='Nuova Offerta',
    #             body=message
    #         )


    def rework_zone(self):
        completed_zone = self.env['cssa.completed_zones'].search([('opportunity_id', '=', self.id)])
        completed_zone.unlink()

        self.user_id = self.env.user.id
        self.to_rework = False

    def register_skype_activity(self):
        skip_activity = False

        for act in self.activity_ids:
            if act.activity_type_id.id == 2 and act.state == 'overdue' and act.state == 'today' and act.state == 'planned':
                skip_activity = True
                break

        if not skip_activity:
            activity = self.env['mail.activity']
            activity.create({
                'res_id': self.id,
                'res_model_id': 199,
                'res_model': 'crm.lead',
                'activity_type_id': 2,
                'date_deadline': date.today(),
                'activity_category': 'default',
                'previous_activity_type_id': False,
                'recommended_activity_type_id': False,
                'user_id': self.user_id.id
            }).action_feedback()

    def action_call_telephone(self):
        if self.phone or self.partner_id.phone:
            self.register_skype_activity()
            if self.partner_id.phone:
                skype_number = 'skype:' + self.partner_id.phone + '?call'
            else:
                skype_number = 'skype:' + self.phone + '?call'
            return {
                'type': 'ir.actions.act_url',
                'url': skype_number,
                'target': 'new',
            }

    # def action_new_appointment(self):
    #     Lead = self.action_assign_new_opportunity(9)
    #     return {
    #         'name': 'Lead',
    #         'view_mode': 'form',
    #         'res_model': 'crm.lead',
    #         'domain': [('type', '=', Lead.type)],
    #         'res_id': Lead.id,
    #         'view_id': False,
    #         'type': 'ir.actions.act_window',
    #         'context': {'default_type': Lead.type}
    #     }

    def _onchange_partner_id_values(self, partner_id):
        """ returns the new values when partner_id has changed """
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)

            partner_name = partner.parent_id.name
            if not partner_name and partner.is_company:
                partner_name = partner.name

            contact_name = False
            contact_title = ''
            contact_function = ''
            if partner.is_company == False:
                contact_name = partner.name
                contact_title = partner.title.id
                contact_function = partner.posizione_lavorativa
            elif len(partner.child_ids) > 0:
                contact_name = partner.child_ids[0].name
                contact_title = partner.child_ids[0].title.id
                contact_function = partner.child_ids[0].posizione_lavorativa

            return {
                'partner_name': partner_name,
                'contact_name': contact_name,
                'title': contact_title,
                'street': partner.street,
                'street2': partner.street2,
                'city': partner.city,
                'state_id': partner.state_id.id,
                'country_id': partner.country_id.id,
                'email_from': partner.email,
                'phone': partner.phone,
                'mobile': partner.mobile,
                'zip': partner.zip,
                'function': partner.function,
                'posizione_lavorativa': contact_function,
                'website': partner.website,
                'dipendenti': partner.dipendenti
            }
        return {}

    def _create_lead_partner_data(self, name, is_company, parent_id=False):
        """ extract data from lead to create a partner
            :param name : furtur name of the partner
            :param is_company : True if the partner is a company
            :param parent_id : id of the parent partner (False if no parent)
            :returns res.partner record
        """
        email_split = tools.email_split(self.email_from)
        res = {
            'name': name,
            'user_id': self.env.context.get('default_user_id') or self.user_id.id,
            'comment': self.description,
            'team_id': self.team_id.id,
            'parent_id': parent_id,
            'phone': self.phone,
            'mobile': self.mobile,
            'email': email_split[0] if email_split else False,
            'title': self.title.id,
            'street': self.street,
            'street2': self.street2,
            'zip': self.zip,
            'city': self.city,
            'country_id': self.country_id.id,
            'state_id': self.state_id.id,
            'website': self.website,
            'is_company': is_company,
            'type': 'contact',
            'provincia': self.provincia,
            'region': self.region,
            'vat': self.partita_iva,
            'partita_iva': self.partita_iva,
            'settore': self.settore,
            'area_interesse': self.area_interesse,
            'ateco': self.ateco,
            'categoria_interesse': self.categoria_interesse,
            'dipendenti': self.dipendenti,
            'fatturato': self.fatturato,
            'function': self.posizione_lavorativa
        }
        if self.lang_id:
            res['lang'] = self.lang_id.code
        return res

    # def manage_in_out(self, stage_id):
    #     if stage_id.id == 10 or stage_id.id == 12:
    #         self.in_out = 'IN'
    #     elif stage_id.id == 11:
    #         self.in_out = 'OUT'

    # def cancel_opportunity(self):
    #     for opportunity in self.partner_id.opportunity_ids:
    #         if opportunity.team_id.id == 9:
    #             self.env['calendar.event'].search([('opportunity_id.id', '=', opportunity.id)]).unlink()
    #             opportunity.unlink()
    #             break

    def action_assign_new_opportunity(self, stage_id):
        value = {
            'planned_revenue': self.planned_revenue,
            'name': self.name,
            'partner_id': self.partner_id.id,
            'type': 'opportunity',
            'date_open': fields.Datetime.now(),
            'email_from': self.partner_id and self.partner_id.email or self.email_from,
            'phone': self.partner_id and self.partner_id.phone or self.phone,
            'date_conversion': fields.Datetime.now(),
            'stage_id': stage_id,
            'activity_ids': None,
            'message_ids': None,
            'settore': self.settore,
            'region': self.region,
            'provincia': self.provincia,
            'ateco': self.ateco,
            'area_interesse': self.area_interesse,
            'categoria_interesse': self.categoria_interesse,
            'tecnico': self.tecnico.id
        }

        current_user = self.env.user

        value['user_id'] = self.tecnico.id
        value['marketer'] = current_user.id
        value['date_meeting'] = self.date_meeting

        value['ext_time'] = self.ext_time
        value['time_meeting'] = self.time_meeting
        value['fatturato'] = self.fatturato
        value['dipendenti'] = self.dipendenti
        value['distance_km'] = self.distance_km

        value['team_id'] = self.tecnico.sale_team_id.id

        value['partita_iva'] = self.partita_iva
        value['codice_fiscale'] = self.codice_fiscale
        value['fatturato'] = self.fatturato

        value['forma_societaria'] = self.forma_societaria
        value['contact_type'] = self.contact_type

        return self.env['crm.lead'].create(value)


    # def confirm_contact_data(self):
    #     self.partner_id.vat = "IT"+self.partita_iva
    #     self.partner_id.partita_iva = "IT"+self.partita_iva
    #     self.partner_id.l10n_it_codice_fiscale = self.codice_fiscale
    #     self.partner_id.fatturato = self.fatturato
    #     self.partner_id.email = self.email_from
    #
    #     self.partner_id.name = self.partner_id.name.upper()
    #     if self.forma_societaria not in self.partner_id.name:
    #         self.partner_id.name = self.partner_id.name + " " + self.forma_societaria


    # @api.onchange('tecnico')
    # def _onchange_tecnico(self):
    #     # update orders if any
    #     self.update_collaborator(self._origin.order_ids, self.tecnico, 121)

    def update_appointment_owner(self):
        for activity in self.activity_ids:
            if activity.calendar_event_id:
                appuntamento = self.env['calendar.event'].search([('id','=',activity.calendar_event_id.id)])

                if self.team_id.id == 5:
                    appuntamento.user_id = self.tecnico.id
                    appuntamento.partner_ids = [self.tecnico.partner_id.id]


    def job_related_users(self, jobid):
        if jobid:
            empids = self.env['hr.employee'].search([('user_id', '!=', False), ('job_id', '=', jobid.id)])
            return [emp.user_id.id for emp in empids]
        return False

    def find_comm_member(self, order, user):
        member_lst = []
        emp_id = self.env['hr.employee'].search([('user_id', '=', user.id)], limit=1)

        for soline in order.order_line:
            for lineid in soline.product_id.product_comm_ids:
                lines_user = {'user_id': user.id, 'job_id': emp_id.job_id.id, 'name_commission': lineid.product_id.name}
                if lineid.user_ids and user.id in [user.id for user in lineid.user_ids]:
                    lines_user[
                        'commission'] = soline.price_subtotal * lineid.commission / 100 if lineid.compute_price_type == 'per' else lineid.commission * soline.product_uom_qty
                    member_lst.append(lines_user)
                elif lineid.job_id and not lineid.user_ids:
                    if user.id in self.job_related_users(lineid.job_id):
                        lines_user[
                            'commission'] = soline.price_subtotal * lineid.commission / 100 if lineid.compute_price_type == 'per' else lineid.commission * soline.product_uom_qty
                        member_lst.append(lines_user)

        return member_lst

    def remove_employee_comm(self, group_id, user_id):
        new_user = self.env['res.users'].search([('id','=',user_id)])

        for order in self._origin.order_ids:
            for commission in order.sale_order_comm_ids:
                for user in commission.user_id:
                    for group in user.groups_id:
                        if group.id == group_id:
                            commission.unlink()

            if new_user:
                member_lst = self.find_comm_member(order, new_user)
                commissione = 0
                for member in member_lst:
                    commissione += member['commission']

                vals = {
                    "user_id": new_user.id,
                    "job_id": new_user.employee_id.job_id.id,
                    "commission": commissione,
                    "order_id": order.id
                }
                self.env['sales.order.commission'].create(vals)
                self.add_new_follower(new_user)


    def update_collaborator(self, orders, collaborator, group_id):
        if collaborator:
            for order in orders:
                member_lst = self.find_comm_member(order, collaborator)

                commission_collaborator = 0
                for member in member_lst:
                    commission_collaborator += member['commission']

                sales_order_commission = self.env['sales.order.commission'].search([('order_id','=',order.id)])
                for commission in sales_order_commission:
                    if commission.job_id.id == collaborator.employee_id.job_id.id:
                        commission.user_id = collaborator.id
                        commission.commission = commission_collaborator
                        break

        else:
            self.remove_employee_comm(group_id, collaborator.id)


    def add_new_follower(self, user):
        follower = self.env['mail.followers'].search(['&',('res_id','=',self._origin.id),('partner_id','=',user.partner_id.id)])
        if not follower:
            reg = {
                'res_id': self._origin.id,
                'res_model': 'crm.lead',
                'partner_id': user.partner_id.id,
            }
            self.env['mail.followers'].create(reg)

    def migrate_leads(self):
        thread = threading.Thread(target=self.migrate, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_leads = new_env['crm.lead'].search([])

                for lead in all_leads:
                    if lead.marketer.name == 'Supporto':
                        lead.marketer = 6

    def migrate_etichette(self):
        thread = threading.Thread(target=self.migrate_e, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()

    def migrate_e(self):
        with api.Environment.manage():
            with openerp.registry(self.env.cr.dbname).cursor() as new_cr:
                new_env = api.Environment(new_cr, self.env.uid, self.env.context)
                all_leads = new_env['crm.lead'].search([])

                for lead in all_leads:
                    for tag in lead.tag_ids:
                        if tag.name == 'Azienda con dati invariati':
                            lead.tag_ids = False
                            lead.contact_type = 'Azienda con dati invariati'
                        elif tag.name == 'Azienda nuova':
                            lead.tag_ids = False
                            lead.contact_type = 'Azienda nuova'
                        elif tag.name == 'Azienda con dati variati':
                            lead.tag_ids = False
                            lead.contact_type = 'Azienda con dati variati'
