# -*- coding: utf-8 -*-

from odoo import models, fields, api
import urllib.parse


class Partner(models.Model):
    _inherit = 'res.partner'

    pratica_legale = fields.Selection([
        ('Inviati documenti','Inviati documenti'),
        ('Decreto ingiuntivo','Decreto ingiuntivo'),
        ('Precetto','Precetto'),
        ('Definizione pratica','Definizione pratica')
    ], 'Pratica Legale')

    relationship = fields.Selection([
        ('Cliente','Cliente'),
        ('Fornitore','Fornitore')
    ], 'Rapporto commerciale')

    pratica_legale_data = fields.Date('Data pratica')

    posizione_lavorativa = fields.Selection([
        ('CEO - SRL/SPA','CEO - SRL/SPA'),
        ('AMMINISTRATORE UNICO - SRL/SPA','AMMINISTRATORE UNICO - SRL/SPA'),
        ('SOCIO ACCOMANDATARIO - SAS','SOCIO ACCOMANDATARIO - SAS'),
        ('PRESIDENTE - SRL/SPA','PRESIDENTE - SRL/SPA'),
        ('TITOLARE - DITTA INDIVIDUALE','TITOLARE - DITTA INDIVIDUALE'),
        ('SOCIO - SNC','SOCIO - SNC'),
        ('AMMINISTRATORE DELEGATO - SRL/SPA','AMMINISTRATORE DELEGATO - SRL/SPA'),
        ('UFF. ACQUISTI','UFF. ACQUISTI'),
        ('UFF. TECNICO','UFF. TECNICO'),
        ('SEGRETARIA/O','SEGRETARIA/O'),
        ('RESP. LABORATORIO','RESP. LABORATORIO'),
        ('RESP. PRODUZIONE','RESP. PRODUZIONE'),
        ('UFF. FORNITORI','UFF. FORNITORI'),
        ('DIPENDENTE','DIPENDENTE'),
        ('LABORATORIO','LABORATORIO'),
        ('ARCHITETTO','ARCHITETTO'),
        ('RESP. INTERIOR','RESP. INTERIOR')
    ])

    fax = fields.Char('Fax')

    dipendenti = fields.Char('N° Dipendenti')
    partita_iva = fields.Char('Partita IVA')
    ateco = fields.Char('ATECO')

    settore = fields.Char('Settore')
    area_interesse = fields.Char("Area d'interesse")
    provincia = fields.Char('Provincia')
    region = fields.Char('Regione')
    sdi = fields.Char('Codice SDI')
    categoria_interesse = fields.Char('Categoria')
    codice_lavoro = fields.Char('Codice Lavoro')

    default_product = fields.Many2one('product.product', string='Prodotto predefinito')

    banca = fields.Char('Banca')
    iban = fields.Char('IBAN')

    fatturato = fields.Float('Ultimo fatturato')

    camere = [
        ('Singola','Singola'),
        ('Matrimoniale Uso Singolo','Matrimoniale Uso Singolo')
    ]
    trattamenti = [
        ('Pernottamento','Pernottamento'),
        ('Colazione','Colazione'),
        ('Mezza pensione','Mezza pensione'),
        ('Pensione completa','Pensione completa')
    ]

    camera_1 = fields.Selection(camere)
    camera_2 = fields.Selection(camere)
    camera_3 = fields.Selection(camere)
    camera_4 = fields.Selection(camere)
    camera_5 = fields.Selection(camere)
    camera_6 = fields.Selection(camere)
    camera_7 = fields.Selection(camere)
    camera_8 = fields.Selection(camere)

    tariffa_1 = fields.Float('Tariffa')
    tariffa_2 = fields.Float('Tariffa')
    tariffa_3 = fields.Float('Tariffa')
    tariffa_4 = fields.Float('Tariffa')
    tariffa_5 = fields.Float('Tariffa')
    tariffa_6 = fields.Float('Tariffa')
    tariffa_7 = fields.Float('Tariffa')
    tariffa_8 = fields.Float('Tariffa')

    trattamento_1 = fields.Selection(trattamenti)
    trattamento_2 = fields.Selection(trattamenti)
    trattamento_3 = fields.Selection(trattamenti)
    trattamento_4 = fields.Selection(trattamenti)
    trattamento_5 = fields.Selection(trattamenti)
    trattamento_6 = fields.Selection(trattamenti)
    trattamento_7 = fields.Selection(trattamenti)
    trattamento_8 = fields.Selection(trattamenti)

    @api.model_create_multi
    def create(self, vals_list):
        if self.env.context.get('import_file'):
            self._check_import_consistency(vals_list)

        for vals in vals_list:
            vals['user_id'] = self.env.user.id
            vals['team_id'] = self.env.user.sale_team_id.id
            vals['company_id'] = self.env.user.company_id.id

            if vals.get('website'):
                vals['website'] = self._clean_website(vals['website'])
            if vals.get('parent_id'):
                vals['company_name'] = False

        partners = super(Partner, self).create(vals_list)

        if self.env.context.get('_partners_skip_fields_sync'):
            return partners

        for partner, vals in zip(partners, vals_list):
            partner._fields_sync(vals)
            partner._handle_first_contact_creation()

        return partners



    def create_opportunity(self):
        value = {
            'planned_revenue': False,
            'probability': 33.79,
            'name': self.name,
            'partner_id': self.id,
            'type': 'opportunity',
            'date_open': fields.Datetime.now(),
            'email_from': self.email,
            'phone': self.phone,
            'mobile': self.mobile,
            'date_conversion': fields.Datetime.now(),
            'user_id': self.env.user.id,
            'settore': self.settore,
            'provincia': self.provincia,
            'region': self.region,
            'area_interesse': self.area_interesse,
            'categoria_interesse': self.categoria_interesse,
            'ateco': self.ateco,
            'partita_iva': self.partita_iva,
            'codice_fiscale': self.l10n_it_codice_fiscale,
            'fatturato': self.fatturato
        }

        Lead = self.env['crm.lead'].create(value)

        return {
            'name': 'Opportunity',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'domain': [('type', '=', Lead.type)],
            'res_id': Lead.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {'default_type': Lead.type}
        }

    def get_directions(self):
        query = self.city+","+self.street+","+self.zip
        encoded_query = urllib.parse.quote(query)
        google_map_url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&destination="+encoded_query

        return {
            'name'     : 'Go to website',
            'res_model': 'ir.actions.act_url',
            'type'     : 'ir.actions.act_url',
            'url'      : google_map_url
        }

    @api.onchange('name')
    def _onchange_name(self):
        existing_partner = self.env['res.partner'].search([('name','ilike',self.name)])
        if self.name and existing_partner:
            return {
                'warning': {
                    'title': 'Attenzione!',
                    'message': 'Esiste un contatto simile. Controllalo prima di continuare.'
                }
            }

    @api.onchange('relationship')
    def _onchange_relationship(self):
        if self.relationship == 'Cliente':
            self._origin.customer_rank = 1
            self._origin.supplier_rank = 0
        elif self.relationship == 'Fornitore':
            self._origin.customer_rank = 0
            self._origin.supplier_rank = 1
        else:
            self._origin.customer_rank = 0
            self._origin.supplier_rank = 0

    @api.onchange('provincia')
    def _onchange_provincia(self):
        provincia = self.env['res.country.state'].search([('code','=',self.provincia)])
        for p in provincia:
            if p.country_id.id == self.country_id.id:
                self.state_id = p.id
                break

    @api.onchange('email')
    def _onchange_email(self):
        for opportunity in self.opportunity_ids:
            opportunity.email_from = self.email
        
    @api.onchange('partita_iva')
    def _onchange_partita_iva(self):
        self._origin.vat = self.partita_iva

    def request_hotel_stay(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Richiesta Pernottamento',
            'res_model': 'hotel.stay.wizard',
            'target': 'new',
            'view_id': self.env.ref('cssa.hotel_stay_wizard').id,
            'view_mode': 'form',
            'context': {'default_partner_id': self.id}
        }


class HotelStay(models.TransientModel):
    _name = 'hotel.stay.wizard'

    partner_id = fields.Integer()

    hotel_email = fields.Char()
    ospiti = fields.Char('Ospiti')

    check_in = fields.Date('Check In')
    check_in_string = fields.Char()
    check_out = fields.Date('Check Out')
    check_out_string = fields.Char()

    n_camere = [
        ('1','1'),
        ('2','2'),
        ('3','3'),
        ('4','4'),
        ('5','5'),
        ('6','6'),
        ('7','7'),
        ('8','8'),
        ('9','9'),
        ('10','10'),
    ]
    n_camera_1 = fields.Selection(n_camere)
    n_camera_2 = fields.Selection(n_camere)

    camere = [
        ('Singola','Singola'),
        ('Matrimoniale Uso Singolo','Matrimoniale Uso Singolo')
    ]
    trattamenti = [
        ('Pernottamento','Pernottamento'),
        ('Colazione','Colazione'),
        ('Mezza pensione','Mezza pensione'),
        ('Pensione completa','Pensione completa')
    ]

    camera_1 = fields.Selection(camere)
    camera_2 = fields.Selection(camere)

    trattamento_1 = fields.Selection(trattamenti)
    trattamento_2 = fields.Selection(trattamenti)

    pagamento = fields.Selection([
        ('Bonifico','Bonifico'),
        ('Carta di credito','Carta di credito')
    ])

    note_pagamento = fields.Char()

    pagamento_string = fields.Char()
    tipologia_camere = fields.Char()
    trattamento = fields.Char()
    tariffa = fields.Char()

    def action_send_stay_request(self):
        partner = self.env['res.partner'].search([('id','=',self.partner_id)])

        self.hotel_email = partner.email

        self.check_in_string = str(self.check_in.day)+'/'+str(self.check_in.month)+'/'+str(self.check_in.year)
        self.check_out_string = str(self.check_out.day)+'/'+str(self.check_out.month)+'/'+str(self.check_out.year)

        self.tipologia_camere = 'n° ' + self.n_camera_1 + ' ' + self.camera_1
        if self.n_camera_2:
            self.tipologia_camere += ', ' + 'n° ' + self.n_camera_2 + ' ' + self.camera_2

        self.trattamento = self.trattamento_1
        if self.trattamento_2 and self.trattamento_1 != self.trattamento_2:
            self.trattamento += ", " + self.trattamento_2

        self.pagamento_string = self.pagamento
        if self.note_pagamento:
            self.pagamento_string += ', ' + self.note_pagamento

        if self.camera_1 == partner.camera_1 and self.trattamento_1 == partner.trattamento_1:
            self.tariffa = '€ ' + str(partner.tariffa_1)
        elif self.camera_1 == partner.camera_2 and self.trattamento_1 == partner.trattamento_2:
            self.tariffa = '€ ' + str(partner.tariffa_2)
        elif self.camera_1 == partner.camera_3 and self.trattamento_1 == partner.trattamento_3:
            self.tariffa = '€ ' + str(partner.tariffa_3)
        elif self.camera_1 == partner.camera_4 and self.trattamento_1 == partner.trattamento_4:
            self.tariffa = '€ ' + str(partner.tariffa_4)
        elif self.camera_1 == partner.camera_5 and self.trattamento_1 == partner.trattamento_5:
            self.tariffa = '€ ' + str(partner.tariffa_5)
        elif self.camera_1 == partner.camera_6 and self.trattamento_1 == partner.trattamento_6:
            self.tariffa = '€ ' + str(partner.tariffa_6)
        elif self.camera_1 == partner.camera_7 and self.trattamento_1 == partner.trattamento_7:
            self.tariffa = '€ ' + str(partner.tariffa_7)
        elif self.camera_1 == partner.camera_8 and self.trattamento_1 == partner.trattamento_8:
            self.tariffa = '€ ' + str(partner.tariffa_8)

        if self.trattamento_2:
            if self.camera_2 == partner.camera_1 and self.trattamento_2 == partner.trattamento_1:
                self.tariffa += ', € ' + str(partner.tariffa_1)
            elif self.camera_2 == partner.camera_2 and self.trattamento_2 == partner.trattamento_2:
                self.tariffa += ', € ' + str(partner.tariffa_2)
            elif self.camera_2 == partner.camera_3 and self.trattamento_2 == partner.trattamento_3:
                self.tariffa += ', € ' + str(partner.tariffa_3)
            elif self.camera_2 == partner.camera_4 and self.trattamento_2 == partner.trattamento_4:
                self.tariffa += ', € ' + str(partner.tariffa_4)
            elif self.camera_2 == partner.camera_5 and self.trattamento_2 == partner.trattamento_5:
                self.tariffa += ', € ' + str(partner.tariffa_5)
            elif self.camera_2 == partner.camera_6 and self.trattamento_2 == partner.trattamento_6:
                self.tariffa += ', € ' + str(partner.tariffa_6)
            elif self.camera_2 == partner.camera_7 and self.trattamento_2 == partner.trattamento_7:
                self.tariffa += ', € ' + str(partner.tariffa_7)
            elif self.camera_2 == partner.camera_8 and self.trattamento_2 == partner.trattamento_8:
                self.tariffa += ', € ' + str(partner.tariffa_8)

        mail_template = self.env.ref('cssa.request_hotel_stay_template')
        mail_template.send_mail(self.id, force_send=True)