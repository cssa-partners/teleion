# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from datetime import date
import calendar


class Installment(models.Model):
    _name = 'installment'

    order_id = fields.Many2one('sale.order', string='Order ID')
    due_date = fields.Date('Scadenza')
    value_amount = fields.Char('Importo')

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    installment_lines = fields.One2many('installment', 'order_id', string='Rate')
    start_contract_date = fields.Date('Data contratto')
    start_work_date = fields.Date('Data inizio lavori')
    assignment_date = fields.Date('Data riaffidamento')
    assignment_amout = fields.Float('Importo riaffidamento')
    coobligato = fields.Char("Coobligato")

    riassigned_user = fields.Many2one('res.users', string='Tecnico', index=True, tracking=True,
                              default=lambda self: self.env.user, domain="['&',('groups_id.category_id.name','=','Tecnico'),('name','=','User')]")

    payment_days = fields.Selection([
        ('5','5'),
        ('10','10'),
        ('15','15'),
        ('20','20'),
        ('fine_mese','Fine mese')

    ], 'Giorno del pagamento')

    commerciale = fields.Many2one('res.users', string='Commerciale', index=True, tracking=True,
                                  default=lambda self: self.env.user, domain="[('sale_team_id','=',9)]")

    commerciale_affiancato = fields.Many2one('res.users', string='Commerciale Affiancato', index=True, tracking=True,
                                             default=lambda self: self.env.user, domain="[('sale_team_id','=',9)]")

    analista = fields.Many2one('res.users', string='Analista', index=True, tracking=True,
                               default=lambda self: self.env.user, domain="[('sale_team_id','=',10)]")

    analista_affiancato = fields.Many2one('res.users', string='Analista Affiancato', index=True, tracking=True,
                                          default=lambda self: self.env.user, domain="[('sale_team_id','=',10)]")

    teleseller = fields.Many2one('res.users', string='Teleseller', index=True, tracking=True,
                                 default=lambda self: self.env.user, domain="[('sale_team_id','=',8)]")


    # Tecnici
    tecnico = fields.Many2one('res.users', string='Tecnico Management', index=True, tracking=True,
                              default=lambda self: self.env.user, domain="[('sale_team_id','=',13)]")

    tutor = fields.Many2one('res.users', string='Tutor Management', index=True, tracking=True,
                            default=lambda self: self.env.user, domain="[('sale_team_id','=',13)]")

    tecnico_certificazioni = fields.Many2one('res.users', string='Tecnico Certificazioni', index=True, tracking=True,
                                             default=lambda self: self.env.user, domain="[('sale_team_id','=',15)]")

    tutor_certificazioni = fields.Many2one('res.users', string='Tutor Certificazioni', index=True, tracking=True,
                                           default=lambda self: self.env.user, domain="[('sale_team_id','=',15)]")

    tecnico_marketing = fields.Many2one('res.users', string='Tecnico Marketing', index=True, tracking=True,
                                        default=lambda self: self.env.user, domain="[('sale_team_id','=',1)]")

    tutor_marketing = fields.Many2one('res.users', string='Tutor Marketing', index=True, tracking=True,
                                      default=lambda self: self.env.user, domain="[('sale_team_id','=',1)]")

    tecnico_software = fields.Many2one('res.users', string='Tecnico Software', index=True, tracking=True,
                                       default=lambda self: self.env.user, domain="[('sale_team_id','=',11)]")

    tutor_software = fields.Many2one('res.users', string='Tutor Software', index=True, tracking=True,
                                     default=lambda self: self.env.user, domain="[('sale_team_id','=',11)]")

    partner_1 = fields.Many2one('res.users', string='Partner 1', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_2 = fields.Many2one('res.users', string='Partner 2', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_3 = fields.Many2one('res.users', string='Partner 3', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_4 = fields.Many2one('res.users', string='Partner 4', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_5 = fields.Many2one('res.users', string='Partner 5', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_6 = fields.Many2one('res.users', string='Partner 6', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_7 = fields.Many2one('res.users', string='Partner 7', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_8 = fields.Many2one('res.users', string='Partner 8', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_9 = fields.Many2one('res.users', string='Partner 9', index=True, tracking=True,
                                default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    partner_10 = fields.Many2one('res.users', string='Partner 10', index=True, tracking=True,
                                 default=lambda self: self.env.user, domain="[('sale_team_id','=',14)]")

    linea_business = fields.Many2one('product.category', string='Linea Business', readonly=True)

    @api.model
    def create(self, values):
        values['message_follower_ids'] = False
        values['user_id'] = self.env.user.id

        sale_order = super(SaleOrder, self).create(values)

        if sale_order.opportunity_id:
            sale_order.commerciale = sale_order.opportunity_id.commerciale
            sale_order.commerciale_affiancato = sale_order.opportunity_id.commerciale_affiancato
            sale_order.analista = sale_order.opportunity_id.analista
            sale_order.analista_affiancato = sale_order.opportunity_id.analista_affiancato
            sale_order.teleseller = sale_order.opportunity_id.teleseller
            sale_order.tecnico = sale_order.opportunity_id.tecnico
            sale_order.tutor = sale_order.opportunity_id.tutor
            sale_order.tecnico_certificazioni = sale_order.opportunity_id.tecnico_certificazioni
            sale_order.tutor_certificazioni = sale_order.opportunity_id.tutor_certificazioni
            sale_order.tecnico_marketing = sale_order.opportunity_id.tecnico_marketing
            sale_order.tutor_marketing = sale_order.opportunity_id.tutor_marketing
            sale_order.tecnico_software = sale_order.opportunity_id.tecnico_software
            sale_order.tutor_software = sale_order.opportunity_id.tutor_software
            sale_order.partner_1 = sale_order.opportunity_id.partner_1
            sale_order.partner_2 = sale_order.opportunity_id.partner_2
            sale_order.partner_3 = sale_order.opportunity_id.partner_3
            sale_order.partner_4 = sale_order.opportunity_id.partner_4
            sale_order.partner_5 = sale_order.opportunity_id.partner_5
            sale_order.partner_6 = sale_order.opportunity_id.partner_6
            sale_order.partner_7 = sale_order.opportunity_id.partner_7
            sale_order.partner_8 = sale_order.opportunity_id.partner_8
            sale_order.partner_9 = sale_order.opportunity_id.partner_9
            sale_order.partner_10 = sale_order.opportunity_id.partner_10

            for line in sale_order.order_line:
                sale_order.linea_business = line.product_id.categ_id.id

        sale_order.riassigned_user = False
        return sale_order

    def calculatePaymentTerms(self):
        if not self.payment_days:
            self.payment_days = '5'

        spese_amount = 0
        for line in self.order_line:
            if line.product_id.categ_id.name == 'Spese':
                spese_amount = line.price_total
                break

        value_amount = self.amount_total
        anticipo = 0
        self.installment_lines = False

        for rec in self:
            lines = []

            n_installments = 0
            for term in self.payment_term_id.line_ids:
                if term.days == 0:
                    anticipo = ((value_amount-spese_amount)*term.value_amount)/100
                    #anticipo = term.value_amount
                else:
                    n_installments += 1

            counter = 0
            for term in self.payment_term_id.line_ids:
                payment_date = self.date_order + datetime.timedelta(days=term.days)
                if self.payment_days != 'fine_mese':
                    payment_date = payment_date.replace(day=int(self.payment_days))
                else:
                    last_day_month = calendar.monthrange(payment_date.year,payment_date.month)[1]
                    payment_date = payment_date.replace(day=last_day_month)

                line_amount = 0
                if term.value == 'fixed':
                    line_amount = term.value_amount
                    vals = {
                        'order_id': self._origin.id,
                        'due_date': payment_date,
                        'value_amount': '€ ' + str(line_amount)
                    }
                    lines.append((0, 0, vals))
                else:
                    if self.payment_term_id.id != 1 and self.payment_term_id.id != 32:
                        counter +=1
                        if term.days == 0:
                            payment_date = self.date_order
                            line_amount = round(anticipo, 2)
                        else:
                            # if counter == n_installments+1:
                            #     line_amount = round(value_amount-anticipo-(line_amount*(n_installments-1)),0)
                            # else:
                            line_amount = round(((value_amount-anticipo)/n_installments),2)

                        vals = {
                            'order_id': self._origin.id,
                            'due_date': payment_date,
                            'value_amount': '€ ' + str(line_amount)
                        }
                        lines.append((0, 0, vals))
                    else:
                        vals = {
                            'order_id': self._origin.id,
                            'due_date': payment_date,
                            'value_amount': '€ ' + str(value_amount)
                        }
                        lines.append((0, 0, vals))

            rec.installment_lines = lines

    def action_confirm(self):
        if self._get_forbidden_state_confirm() & set(self.mapped('state')):
            raise UserError(_(
                'It is not allowed to confirm an order in the following states: %s'
            ) % (', '.join(self._get_forbidden_state_confirm())))

        self.write({
            'state': 'sale',
            'date_order': fields.Datetime.now()
        })
        self._action_confirm()
        if self.env.user.has_group('sale.group_auto_done_setting'):
            self.action_done()
        return True

    @api.onchange('order_line')
    def _onchange_order_line(self):
        if self.payment_term_id:
            self.calculatePaymentTerms()

        for line in self.order_line:
            if 'Check' in line.product_id.name or 'Go' in line.product_id.name:
                if 'effettuato' not in line.name:
                    line.name = line.name + " effettuato in data "
                    break

    @api.onchange('payment_term_id')
    def _onchange_payment_term_id(self):
        if self.order_line:
            self.calculatePaymentTerms()

    @api.onchange('payment_days')
    def _onchange_payment_days(self):
        if self.payment_term_id:
            self.calculatePaymentTerms()